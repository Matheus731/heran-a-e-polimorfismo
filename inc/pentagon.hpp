#ifndef PENTAGON_HPP
#define PENTAGON_HPP
#include "formageometrica.hpp"
using namespace std;
class Pentagon : public FormaGeometrica{
private:
    float side;
public:
    Pentagon(string type,float side);
    ~Pentagon();
    float calcula_area();
    float calcula_perimetro();
    float get_side();
    void set_side(float side);
};
#endif

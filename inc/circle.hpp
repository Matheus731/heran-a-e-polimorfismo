#ifndef CIRCLE_HPP
#define CIRCLE_HPP
#include "formageometrica.hpp"
using namespace std;
class Circle : public FormaGeometrica{
private:
    float radius;
public:
    Circle(string type,float radius);
    ~Circle();
    float calcula_area();
    float calcula_perimetro();
    float get_radius();
    void set_radius(float radius);
    
};

#endif

#ifndef HEXAGON_HPP
#define HEXAGON_HPP
#include "formageometrica.hpp"
using namespace std;
class Hexagon : public FormaGeometrica{
private: 
    float side;
public: 
    Hexagon(string type,float side);
    ~Hexagon();
    float calcula_area();
    float calcula_perimetro();
    float get_side();
    void set_side(float side);
};
#endif

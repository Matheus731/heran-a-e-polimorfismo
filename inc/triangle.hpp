#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "formageometrica.hpp"

class Triangle : public FormaGeometrica{
public:
    Triangle(string type,float base,float heigth);
    ~Triangle();
    float calcula_area();
    float calcula_perimetro();

};


#endif

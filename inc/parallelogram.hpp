#ifndef PARALLELOGRAM_HPP
#define SQUARE_HPP

#include "formageometrica.hpp"
using namespace std;
class Parallelogram : public FormaGeometrica{

public:
    Parallelogram(string type,float base,float heigth);
    ~Parallelogram();
    float calcula_area();
    float calcula_perimetro();
};
#endif

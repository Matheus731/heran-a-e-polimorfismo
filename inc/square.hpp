#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "formageometrica.hpp"
using namespace std;
class Square : public FormaGeometrica{

public:
    Square(string type,float base, float height);        
    ~Square();
        float calcula_area();
        float calcula_perimetro();




};



#endif

#include "parallelogram.hpp"
#include "formageometrica.hpp"
#include <iostream>

Parallelogram::Parallelogram(string type,float base,float heigth){
    set_tipo(type);
    set_base(base);
    set_altura(heigth);

}
float Parallelogram::calcula_area(){
//    cout<<"used method of parallelogram"<<endl;
        return get_base()*get_altura();
}
float Parallelogram::calcula_perimetro(){
    return 2*get_base()+2*get_altura();
}

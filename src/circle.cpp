#include "circle.hpp"
#include "formageometrica.hpp"
#include <iostream>
#include <cmath>

using namespace std;
Circle::Circle(string type,float radius){
    set_tipo(type);
    set_radius(radius);

}
    Circle::~Circle(){}

float Circle::calcula_area(){
//    cout<<"Circle method was used"<<endl;
    return 3.1415*pow(get_radius(),2.0);
}
float Circle::calcula_perimetro(){
    return 2*3.1415*get_radius();
}
float Circle::get_radius(){
    return radius;
}
void Circle::set_radius(float radius){
    this-> radius=radius;
}

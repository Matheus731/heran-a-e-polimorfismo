#include "square.hpp"
#include "formageometrica.hpp"
#include <iostream>

using namespace std;
Square::Square(string type,float base, float heigth){
    set_tipo(type);
    set_base(base);
    set_altura(heigth);
}

float Square::calcula_area(){
//    cout<<"Square method used"<<endl;

    return get_base()*get_altura();
}

float Square::calcula_perimetro(){
    return 2*get_base()+2*get_altura();
}

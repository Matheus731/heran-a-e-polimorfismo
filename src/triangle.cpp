#include "triangle.hpp"
#include "formageometrica.hpp"
#include <cmath>
Triangle::Triangle(string type,float base,float heigth){
    set_tipo(type);
    set_base(base);
    set_altura(heigth);
}

float Triangle::calcula_area(){
    return (get_base()*get_altura())/2;
}

float Triangle::calcula_perimetro(){
    float hip=sqrt((pow(get_base(),2.0))+(pow(get_altura(),2.0)));
    return hip+get_base()+get_altura();
}

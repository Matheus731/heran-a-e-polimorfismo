#include <iostream>
#include "formageometrica.hpp"
#include "square.hpp"
#include "triangle.hpp"
#include <vector>
#include "parallelogram.hpp"
#include "pentagon.hpp"
#include "hexagon.hpp"
#include "circle.hpp"
int main(){
    vector<FormaGeometrica*> list;
//    cout<< "Inicio da main"<<endl;

    list.emplace_back(new Square("square",10.0,10.0));
    list.emplace_back( new Triangle("triangle",3.0,4.0));
    list.emplace_back( new Parallelogram("parallelogram",5.0,6.0));
    list.emplace_back(new Pentagon("pentagon",4.0));
    list.emplace_back( new Hexagon("hexagon",8.0));
    list.emplace_back(new Circle("circle",12.0));
         
    for(size_t cont=0;cont<list.size();cont++)
        cout<<list[cont]->get_tipo()<<" Area: "<<list[cont]->calcula_area()<<" Perimeter: "<<list[cont]->calcula_perimetro()<<endl;

    return 0;
}

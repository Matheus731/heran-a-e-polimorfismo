#include "hexagon.hpp"
#include "formageometrica.hpp"
#include <iostream>
#include <cmath>

Hexagon::Hexagon(string type,float side){
    set_tipo(type);
    set_side(side);

}
Hexagon::~Hexagon(){};
float Hexagon::calcula_area(){
//    cout<<"Hexagon method was used"<<endl;
    float area_triangle;
    area_triangle = pow(get_side(),2.0)*(sqrt(3.0)/4);

    return area_triangle*6.0;
}
float Hexagon::calcula_perimetro(){
    return 6*get_side();

}
float Hexagon::get_side(){
    return side;
}

void Hexagon::set_side(float side){
    this -> side=side;
}

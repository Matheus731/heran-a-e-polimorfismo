#include "pentagon.hpp"
#include "formageometrica.hpp"
#include <iostream>

Pentagon::Pentagon(string type,float side){
    set_tipo(type);
    set_side(side);

}

Pentagon::~Pentagon(){}

float Pentagon::calcula_area(){
//    cout<<"Pentagon method used"<<endl;
    return (2.0/0.727)*Pentagon::calcula_perimetro()/2;

}
float Pentagon::calcula_perimetro(){
    return 5*get_base();
}
float Pentagon::get_side(){
    return side;
}
void Pentagon::set_side(float side){
    this-> side=side;
}
